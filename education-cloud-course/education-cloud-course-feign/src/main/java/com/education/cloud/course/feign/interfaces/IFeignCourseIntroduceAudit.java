package com.education.cloud.course.feign.interfaces;

import com.education.cloud.course.feign.qo.CourseIntroduceAuditQO;
import com.education.cloud.course.feign.vo.CourseIntroduceAuditVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程介绍信息
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.COURSE_SERVICE,contextId = "courseChapterIntroduceAuditClient")
public interface IFeignCourseIntroduceAudit {

    @RequestMapping(value = "/feign/course/courseIntroduceAudit/listForPage", method = RequestMethod.POST)
    Page<CourseIntroduceAuditVO> listForPage(@RequestBody CourseIntroduceAuditQO qo);

    @RequestMapping(value = "/feign/course/courseIntroduceAudit/save", method = RequestMethod.POST)
    int save(@RequestBody CourseIntroduceAuditQO qo);

    @RequestMapping(value = "/feign/course/courseIntroduceAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/course/courseIntroduceAudit/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseIntroduceAuditQO qo);

    @RequestMapping(value = "/feign/course/courseIntroduceAudit/get/{id}", method = RequestMethod.GET)
    CourseIntroduceAuditVO getById(@PathVariable(value = "id") Long id);

}
