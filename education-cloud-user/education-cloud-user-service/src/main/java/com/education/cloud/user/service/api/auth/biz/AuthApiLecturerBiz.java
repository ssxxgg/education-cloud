package com.education.cloud.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.education.cloud.user.service.dao.LecturerDao;
import com.education.cloud.user.service.dao.impl.mapper.entity.Lecturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.user.common.bo.LecturerViewBO;
import com.education.cloud.user.common.dto.LecturerViewDTO;
import com.education.cloud.user.service.dao.LecturerDao;
import com.education.cloud.user.service.dao.impl.mapper.entity.Lecturer;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Component
public class AuthApiLecturerBiz {

	@Autowired
	private LecturerDao lecturerDao;

	/**
	 * 讲师信息查看接口
	 *
	 * @param lecturerUserNo
	 * @author wuyun
	 */
	public Result<LecturerViewDTO> view(LecturerViewBO lecturerViewBO) {
		if (null == lecturerViewBO.getLecturerUserNo()) {
			return Result.error("讲师编号不能为空");
		}
		Lecturer lecturer = lecturerDao.getByLecturerUserNo(lecturerViewBO.getLecturerUserNo());
		if (ObjectUtil.isNull(lecturer)) {
			return Result.error("找不到该讲师");
		}
		return Result.success(BeanUtil.copyProperties(lecturer, LecturerViewDTO.class));
	}
}
