package com.education.cloud.user.service.feign;

import com.education.cloud.user.feign.interfaces.IFeignLecturerProfit;
import com.education.cloud.user.feign.qo.LecturerProfitQO;
import com.education.cloud.user.feign.vo.LecturerProfitVO;
import com.education.cloud.user.service.feign.biz.FeignLecturerProfitBiz;
import com.education.cloud.util.base.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.education.cloud.util.base.BaseController;

/**
 * 讲师提现日志表
 *
 * @author wujing
 */

@Api(value = "讲师提现日志", tags = "讲师提现日志")
@RestController
public class FeignLecturerProfitController extends BaseController implements IFeignLecturerProfit {

	@Autowired
	private FeignLecturerProfitBiz biz;

	@Override
	public Page<LecturerProfitVO> listForPage(@RequestBody LecturerProfitQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody LecturerProfitQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@PathVariable(value = "id") Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody LecturerProfitQO qo){
		return biz.updateById(qo);
	}

    @Override
	public LecturerProfitVO getById(@PathVariable(value = "id") Long id){
		return biz.getById(id);
	}
}
