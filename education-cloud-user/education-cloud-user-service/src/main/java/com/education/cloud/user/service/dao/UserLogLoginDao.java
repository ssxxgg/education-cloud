package com.education.cloud.user.service.dao;

import com.education.cloud.user.service.dao.impl.mapper.entity.UserLogLogin;
import com.education.cloud.user.service.dao.impl.mapper.entity.UserLogLoginExample;
import com.education.cloud.user.service.dao.impl.mapper.entity.UserLogLogin;
import com.education.cloud.user.service.dao.impl.mapper.entity.UserLogLoginExample;
import com.education.cloud.util.base.Page;

public interface UserLogLoginDao {
    int save(UserLogLogin record);

    int deleteById(Long id);

    int updateById(UserLogLogin record);

    UserLogLogin getById(Long id);

    Page<UserLogLogin> listForPage(int pageCurrent, int pageSize, UserLogLoginExample example);
}
