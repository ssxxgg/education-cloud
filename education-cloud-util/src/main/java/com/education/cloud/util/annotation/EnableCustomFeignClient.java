package com.education.cloud.util.annotation;

import com.education.cloud.util.config.CustomFeignConfig;
import com.education.cloud.util.config.CustomFeignConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description 自定义feign参数传递
 * @Date 2020/3/22
 * @Created by 67068
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(CustomFeignConfig.class)
public @interface EnableCustomFeignClient {
}
